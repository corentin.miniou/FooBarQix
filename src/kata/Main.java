package kata;

public class Main {

    private final static String FOO = "Foo";
    private final static String BAR = "Bar";
    private final static String QIX = "Qix";
    private final static String STAR = "*";

    public static String compute(String number){
        //Convert into number
        long numberInt = Long.parseLong(number);
        var str = isDivisible(numberInt);
        //Replace each 3,5,7 by Foo, Bar, Qix
        var numberCalc = number;
        numberCalc = numberCalc.replaceAll("3",FOO);
        numberCalc = numberCalc.replaceAll("5",BAR);
        numberCalc = numberCalc.replaceAll("7",QIX);
        //US 2 : Replace 0 by *
        numberCalc = numberCalc.replaceAll("0",STAR);
        //check if the string has been modified
        boolean isModified = numberCalc.contains(FOO) || numberCalc.contains(BAR) || numberCalc.contains(QIX);
        //In case it's modified or divisible by 3,5, 7
        if(!str.isEmpty() || isModified) {
            //we replace each digit by an empty string
            numberCalc = numberCalc.replaceAll("\\d", "");
        }

        return str+numberCalc;
    }

    /**
     * Check if the number is divisible by 3, 5, 7 and returning “Foo”, “Bar”, “Qix”
     * @param number Number to check
     * @return Return the word matching the divisibility
     */
    public static String isDivisible(long number){
        var result = "";
        if (number %  3 == 0){
            result+= FOO;
        }
        if  (number %  5 == 0){
            result+= BAR;
        }
        if (number %  7 == 0){
            result+= QIX;
        }

        return result;
    }

    public static void main(String[] args) {
        System.out.println(1 + " " + compute("1"));
        System.out.println(2 + " " + compute("2"));
        System.out.println(3 + " " + compute("3"));
        System.out.println(4 + " " + compute("4"));
        System.out.println(5 + " " + compute("5"));
        System.out.println(6 + " " + compute("6"));
        System.out.println(7 + " " + compute("7"));
        System.out.println(8 + " " + compute("8"));
        System.out.println(9 + " " + compute("9"));
        System.out.println(10 + " " + compute("10"));
        System.out.println(13 + " " + compute("13"));
        System.out.println(15 + " " + compute("15"));
        System.out.println(21 + " " + compute("21"));
        System.out.println(33 + " " + compute("33"));
        System.out.println(51 + " " + compute("51"));
        System.out.println(53 + " " + compute("53"));
        System.out.println(101 + " " + compute("101"));
        System.out.println(303 + " " + compute("303"));
        System.out.println(105 + " " + compute("105"));
        System.out.println(10101 + " " + compute("10101"));
    }
}
